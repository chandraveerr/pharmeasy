package com.pharmeasy.test.interfaces;

public interface OnRecyclerItemClickListener {

    void onItemClick(int position);
}
