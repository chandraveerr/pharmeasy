package com.pharmeasy.test.view;

import com.pharmeasy.test.R;
import com.pharmeasy.test.databinding.ChildDetailBinding;
import com.pharmeasy.test.adapter.model.Geoname;

import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.view.MenuItem;

/**
 * Created by chandraveer on 1/15/17.
 */

public class DetailActivity extends AppCompatActivity {

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        ChildDetailBinding binding = DataBindingUtil.setContentView(this, R.layout.child_detail);
        binding.setGeoname((Geoname) getIntent().getSerializableExtra("geoname"));
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
        // Respond to the action bar's Up/Home button
        case android.R.id.home:
            finish();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }
}
