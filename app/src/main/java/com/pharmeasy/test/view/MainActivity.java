package com.pharmeasy.test.view;

import com.pharmeasy.test.R;
import com.pharmeasy.test.adapter.MyAdapter;
import com.pharmeasy.test.interfaces.OnRecyclerItemClickListener;
import com.pharmeasy.test.presenter.MainPresenter;
import com.pharmeasy.test.presenter.MainPresenterImp;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;

import butterknife.Bind;
import butterknife.ButterKnife;

/**
 * Created by chandraveer on 1/15/17.
 */

public class MainActivity extends AppCompatActivity {

    @Bind(R.id.recyle_view)
    RecyclerView                mRecyclerView;

    private MainPresenter       mPresenter;

    private MyAdapter           mAdapter;

    private LinearLayoutManager mLayoutManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);
        mRecyclerView.setHasFixedSize(true);

        // use a linear layout manager
        mLayoutManager = new LinearLayoutManager(this);
        mRecyclerView.setLayoutManager(mLayoutManager);

        mAdapter = new MyAdapter(this);
        mPresenter = new MainPresenterImp(this, mAdapter);
        mAdapter.setOnRecyclerItemClickListener(/*
                                                 * position -> {
                                                 * mPresenter.onListItemClick(
                                                 * position); }
                                                 */new OnRecyclerItemClickListener() {

            @Override
            public void onItemClick(int position) {
                mPresenter.onListItemClick(position);
            }
        });
        mRecyclerView.setAdapter(mAdapter);
        mPresenter.loadData();
    }

}
