package com.pharmeasy.test.presenter;

import java.util.List;

import android.content.Context;
import android.content.Intent;
import android.util.Log;

import com.pharmeasy.test.adapter.MyAdapter;
import com.pharmeasy.test.adapter.model.Geoname;
import com.pharmeasy.test.adapter.model.Geonames;
import com.pharmeasy.test.network.MyApiEndpointInterface;
import com.pharmeasy.test.network.RetrofitCreator;
import com.pharmeasy.test.view.DetailActivity;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;

/**
 * Created by chandraveer on 1/15/17.
 */

public class MainPresenterImp implements MainPresenter {

    private final static String LOG_TAG = "MainPresenterImp";

    private Context             mContext;

    private MyAdapter mAdapter;

    private List<Geoname>       mData;

    public MainPresenterImp(Context context, MyAdapter adapter) {
        mContext = context;
        mAdapter = adapter;
    }

    @Override
    public void loadData() {
        Retrofit retrofit = RetrofitCreator.provideRetrofit();
        MyApiEndpointInterface apiService = retrofit.create(MyApiEndpointInterface.class);
        Call<Geonames> call = apiService.getGeonames("citiesJSON", "44", "-9.9", "-22.4", "55.2", "de", "chandraveer");
        call.enqueue(new Callback<Geonames>() {

            @Override
            public void onResponse(Call<Geonames> call, Response<Geonames> response) {
                mData = response.body().getGeonames();
                mAdapter.addData(response.body().getGeonames());
                mAdapter.notifyDataSetChanged();
            }

            @Override
            public void onFailure(Call<Geonames> call, Throwable t) {
                Log.e(LOG_TAG, "network call failed");
            }
        });
    }

    @Override
    public void onListItemClick(int position) {
        Intent i = new Intent(mContext, DetailActivity.class);
        i.putExtra("geoname", mData.get(position));
        mContext.startActivity(i);
    }
}
