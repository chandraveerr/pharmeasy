package com.pharmeasy.test.presenter;

/**
 * Created by chandraveer on 1/15/17.
 */

public interface MainPresenter {

    void loadData();

    void onListItemClick(int position);
}
