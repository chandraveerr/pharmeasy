package com.pharmeasy.test.adapter;

import java.util.ArrayList;
import java.util.List;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.pharmeasy.test.adapter.model.Geoname;
import com.pharmeasy.test.R;
import com.pharmeasy.test.interfaces.OnRecyclerItemClickListener;

import butterknife.Bind;
import butterknife.ButterKnife;

/**
 * Created by chandraveer on 1/15/17.
 */

public class MyAdapter extends RecyclerView.Adapter<MyAdapter.ViewHolder> {

    private List<Geoname>               mDataset;

//    private Context                     mContext;

    private OnRecyclerItemClickListener onRecyclerItemClickListener;

    // Provide a suitable constructor (depends on the kind of dataset)
    public MyAdapter(/* List<Geoname> myDataset */Context context) {
//        mContext = context;
        mDataset = new ArrayList<>();
    }

    // Create new views (invoked by the layout manager)
    @Override
    public MyAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        // create a new view
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.child_item, parent, false);
        ViewHolder vh = new ViewHolder(v);
        return vh;
    }

    // Replace the contents of a view (invoked by the layout manager)
    @Override
    public void onBindViewHolder(ViewHolder holder, final int position) {
        // - get element from your dataset at this position
        // - replace the contents of the view with that element
        holder.mTextView.setText(mDataset.get(position).getName());
        holder.itemView.setOnClickListener(/*
                                            * view -> {
                                            * onRecyclerItemClickListener.
                                            * onItemClick(position); }
                                            */new View.OnClickListener() {

            @Override
            public void onClick(View view) {
                onRecyclerItemClickListener.onItemClick(position);
            }
        });

    }

    // Return the size of your dataset (invoked by the layout manager)
    @Override
    public int getItemCount() {
        return mDataset.size();
    }

    public void addData(List<Geoname> myDataset) {
        if (myDataset != null) {
            mDataset.addAll(myDataset);
        }
    }

    public void setOnRecyclerItemClickListener(OnRecyclerItemClickListener onRecyclerItemClickListener) {
        this.onRecyclerItemClickListener = onRecyclerItemClickListener;
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {

        // each data item is just a string in this case
        @Bind(R.id.textView)
        TextView mTextView;

        public ViewHolder(View v) {
            super(v);
            ButterKnife.bind(this, v);
        }
    }

}
