package com.pharmeasy.test.network;

import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by chandraveer on 1/14/17.
 */

public class RetrofitCreator {

    public static Retrofit provideRetrofit() {

        final String BASE_URL = "http://api.geonames.org/";
        Retrofit retrofit = new Retrofit.Builder().baseUrl(BASE_URL).addConverterFactory(GsonConverterFactory.create()).build();
        return retrofit;
    }
}
