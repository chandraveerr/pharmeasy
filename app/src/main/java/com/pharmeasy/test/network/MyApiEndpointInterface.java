package com.pharmeasy.test.network;

import com.pharmeasy.test.adapter.model.Geonames;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Path;
import retrofit2.http.Query;

/**
 * Created by chandraveer on 1/13/17.
 */

public interface MyApiEndpointInterface {

    @GET("{data}")
    Call<Geonames> getGeonames(@Path("data") String data, @Query("north") String north, @Query("south") String south, @Query("east") String east, @Query("west") String west, @Query("lang") String lang, @Query("username") String username);

//    @GET("{data}")
//    Call<ErrorResponse> getError(@Path("data") String data);
}
